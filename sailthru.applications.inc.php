<?php

/**
 * @file
 * Admin screens for the main Sailthru tabs.
 */

/**
 * Scout Configuration.
 */
function sailthru_scout_config() {

  $form = array();
  $st_ready = sailthru_validate_install();
  sailthru_check_sailthru();

  if ($st_ready) {

    $form['sailthru_scout'] = array(
      '#type' => 'fieldset',
      '#title' => t('Scout Configuration'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $markup = '<p>Scout is a powerful recommendation engine that, when applied to your website, automatically curates your content, making it personalized and relevant for each individual user. See the <a href="http://getstarted.sailthru.com/documentation/products/scout" target="_blank">Scout documentation</a> for configuration options.</p>';

    $form['sailthru_scout']['intro'] = array(
      '#markup' => t($markup),
    );

    $scout_enabled = variable_get('sailthru_scout_enabled', FALSE);

    $form['sailthru_scout']['sailthru_scout_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Scout'),
      '#required' => FALSE,
      '#value' => 1,
      '#default_value' => $scout_enabled,
    );

    $scout_num = 20;
    $i = 1;
    while ($i <= $scout_num) {
      $scout_options[$i] = $i;
      $i++;
    }

    $form['sailthru_scout']['sailthru_scout_num_items'] = array(
      '#type' => 'select',
      '#title' => t('Number of visible items'),
      '#description' => t('The number of items to render at a time. The default is 10'),
      '#required' => FALSE,
      '#options' => $scout_options,
      '#default_value' => variable_get('sailthru_scout_num_items', '10'),
    );

    $form['sailthru_scout']['sailthru_scout_include_consumed'] = array(
      '#type' => 'select',
      '#title' => t('Display items that have already been seen by the user'),
      '#description' => t('Select show to include content that has already been consumed by the user'),
      '#required' => FALSE,
      '#options' => array('true' => 'Show', 'false' => 'Do not show'),
      '#default_value' => variable_get('sailthru_scout_include_consumed', 'false'),
    );

    $form['sailthru_scout']['sailthru_scout_render'] = array(
      '#type' => 'textarea',
      '#title' => t("Override scout's rendering function"),
      '#description' => t(''),
      '#required' => FALSE,
      '#rows' => 10,
      '#cols' => 45,
      '#default_value' => variable_get('sailthru_scout_render', ''),
      '#prefix' => t('You can fully customize the HTML for Scout. See the <a href="http://getstarted.sailthru.com/documentation/products/scout" target="_blank">Scout documentation</a> for details.'),
    );
    return system_settings_form($form);

  }
  else {
    $form['sailthru'] = array(
      '#type' => 'fieldset',
      '#title' => t('Sailthru Configuration'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['sailthru']['message'] = array(
      '#markup' => t('<p>Sailthru is not configured, <a href="/admin/sailthru">add your Sailthru API Key,  Secret and make sure you\'ve selected an email template.</a></p>'),
    );
    return $form;
  }
}

/**
 * Concierge configuration.
 */
function sailthru_concierge_config() {

  $form = array();
  $st_ready = sailthru_validate_install();

  if ($st_ready) {
    $form['sailthru_concierge'] = array(
      '#type' => 'fieldset',
      '#title' => t('Concierge Configuration'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['sailthru_concierge']['intro'] = array(
      '#markup' => t('<p>The Sailthru Concierge slider is an on-site recommendation widget that recommends content relevant to the user. Get more information on our <a href="http://docs.sailthru.com/documentation/products/concierge">Sailthru Concierge documentation</a> page. </p>'),
    );

    $concierge_enabled = variable_get('sailthru_concierge_enabled', FALSE);

    $form['sailthru_concierge']['sailthru_concierge_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Concierge'),
      '#required' => FALSE,
      '#value' => 1,
      '#default_value' => $concierge_enabled,
    );

    $form['sailthru_concierge']['sailthru_concierge_delay'] = array(
      '#type' => 'select',
      '#title' => t('Display Delay'),
      '#description' => t('By what time to delay concierge recommendation box in milliseconds; not delayed by default'),
      '#required' => FALSE,
      '#options' => array(
        '0' => t('No Delay'),
        '100' => t('100 Milliseconds'),
        '250' => t('250 Milliseconds'),
        '500' => t('500 Milliseconds'),
        '750' => t('750 Milliseconds'),
        '1000' => t('1 Second'),
        '2000' => t('2 Seconds'),
        '3000' => t('3 Seconds'),
        '4000' => t('4 Seconds'),
        '5000' => t('5 Seconds'),
        '10000' => t('10 Seconds'),
      ),
      '#default_value' => variable_get('sailthru_concierge_from', ''),
    );

    $form['sailthru_concierge']['sailthru_concierge_threshold'] = array(
      '#type' => 'textfield',
      '#title' => t('Threshold'),
      '#description' => t('A lower threshold value means the box will display within shorter page / mouse scroll and vice-versa. You can also pass a jQuery page object instead.'),
      '#required' => FALSE,
      '#default_value' => variable_get('sailthru_concierge_threshold', ''),
    );

    $form['sailthru_concierge']['sailthru_concierge_tags'] = array(
      '#type' => 'textfield',
      '#title' => t('Filter'),
      '#description' => t('To only return content tagged a certain way add tags separated by a comma'),
      '#required' => FALSE,
      '#default_value' => '' . variable_get('sailthru_concierge_tags', '') . '',
    );

    $css_path = ' https://ak.sail-horizon.com/horizon/recommendation.css';

    $form['sailthru_concierge']['sailthru_concierge_css_path'] = array(
      '#type' => 'textfield',
      '#title' => t('CSS file path'),
      '#description' => t('Provide the path for the css from the web site root path'),
      '#required' => FALSE,
      '#default_value' => variable_get('sailthru_concierge_css_path', $css_path),
    );

    $form['sailthru_concierge']['sailthru_concierge_from'] = array(
      '#type' => 'select',
      '#title' => t('Display Location'),
      '#description' => t('Display location of the recommendation box (default is bottom).'),
      '#options' => array(
        'bottom' => t('Bottom'),
        'top' => t('Top'),
      ),
      '#required' => FALSE,
      '#default_value' => variable_get('sailthru_concierge_from', 'bottom'),
    );

    $form['sailthru_concierge']['sailthru_concierge_paths'] = array(
      '#type' => 'textarea',
      '#title' => t('Display Paths'),
      '#description' => t("Configure Concierge's display paths"),
      '#required' => FALSE,
      '#default_value' => variable_get('sailthru_concierge_paths', ''),
      '#suffix'   => '<p>To display concierge on specific paths in your site configure each path on a new line in the field above. For all paths below a specific url use the wildcard *. For example blog only matches http://yoursite.com/blog but adding blog/* will match http://yoursite.com/blog/blog1 and http://yoursite.com/blog/blog2',
    );

    return system_settings_form($form);

  }
  else {
    $form['sailthru'] = array(
      '#type' => 'fieldset',
      '#title' => t('Sailthru Configuration'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['sailthru']['message'] = array(
      '#markup' => t('<p>Sailthru is not configured, <a href="/admin/sailthru">add your Sailthru API Key,  Secret and make sure you\'ve selected an email template.</a></p>'),
    );
  }

  return $form;

}

/**
 * Provides Configuration Form.
 */
function sailthru_apps_config_form() {

  $form = array();
  $sailthru = sailthru_get_client();
  $options = sailthru_get_templates($sailthru);
  $st_ready = sailthru_validate_install();

  if ($st_ready) {
    if (!$sailthru) {
      $form['sailthru_signup']['message'] = array(
        '#markup' => t("<p>You have not setup your Sailthru API key and secret, this is a required step before sending emails</p>"),
        '#weight' => '-20',
      );
    }

    $form['sailthru_subscribe'] = array(
      '#type' => 'fieldset',
      '#title' => t('Newsletter Signup'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    try {
      $lists = $sailthru->apiGet('list', array());
      $options = array('');
      foreach ($lists['lists'] as $list) {
        $options[$list['name']] = $list['name'];
      }

      $form['sailthru_subscribe']['user_subscribe_list'] = array(
        '#type' => 'select',
        '#title' => t('Add subscribers to list'),
        '#options' => $options,
        '#default_value' => variable_get('sailthru_user_subscribe_list', ''),
      );

      $form['sailthru_subscribe']['user_subscribe_markup'] = array(
        '#markup' => '<p>By default this form will collect the user email address, Collect first and last name from users check the boxes below.</p>',
      );

      $form['sailthru_subscribe']['user_subscribe_name_fields'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable first & last name fields'),
        '#default_value' => variable_get('sailthru_user_subscribe_name_fields', ''),
      );
    }
    catch (Sailthru_Client_Exception $e) {
      watchdog('Sailthru - Lists', 'Could not get lists from Sailthru ' . $e);
      drupal_set_message("<strong>Oh Snap!</strong> I couldn't get the lists from Sailthru");
    }
    return system_settings_form($form);

  }
  else {
    $form['sailthru'] = array(
      '#type' => 'fieldset',
      '#title' => t('Sailthru Configuration'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['sailthru']['message'] = array(
      '#markup' => t('<p>Sailthru is not configured, <a href="/admin/sailthru">add your Sailthru API Key,  Secret and make sure you\'ve selected an email template.</a></p>'),
    );
    return $form;
  }

}

/**
 * Gets the email subscription lists from Sailthru.
 */
function sailthru_lists($form, &$form_state, $list_name = '') {

  $form = array();
  $st_ready = sailthru_validate_install();

  if ($st_ready) {
    if ($list_name != '') {
      $form['list_details'] = array(
        '#type' => 'fieldset',
        '#title' => t('List Details'),
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
      );

      $form['list_details']['list_info'] = array(
        '#markup' => sailthru_build_list_details_ui($list_name),
      );
    }

    $form['lists'] = array(
      '#type' => 'fieldset',
      '#title' => t('Lists'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['lists']['intro'] = array(
      '#markup' => t('<p>You have the following lists on your account.To manage smart lists and subscribers or use the query builder please login to your <a href="https://my.sailthru.com/lists">Sailthru dashboard.</a></p>'),
    );

    $form['lists']['ui'] = array(
      '#markup' => sailthru_build_list_table_ui(),
    );
  }
  else {
    $form['sailthru_concierge'] = array(
      '#type' => 'fieldset',
      '#title' => t('Concierge Configuration'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['sailthru']['message'] = array(
      '#markup' => t('<p>Sailthru is not configured, <a href="/admin/sailthru">add your Sailthru API Key,  Secret and make sure you\'ve selected an email template.</a></p>'),
    );
  }
  return $form;
}

/**
 * Gets the HTML for scout.
 */
function sailthru_get_scout_page() {
  $html = '<div id="sailthru-scout"><div class="loading">' . t('Loading, please wait') . '</div></div>';
  return $html;
}

/**
 * Instructions for downloading the client library.
 */
function sailthru_install_download() {
  $text = file_get_contents(dirname(__FILE__) . "/library_instructions.html");
  $html = '<div id="sailthru-instructions">' . t($text) . '</div>';
  return $html;
}

/**
 * Builds out the UI for the list table.
 */
function sailthru_build_list_table_ui() {

  $html = '';

  try {
    $sailthru = sailthru_get_client();
    if ($sailthru) {
      $lists = $sailthru->apiGet('list', array());
      $html .= '<table><tr><th>List Name</th><th>Created</th><th>List Count</th></tr>';
      foreach ($lists['lists'] as $list) {
        $html .= '<tr><td><a href="/admin/sailthru/lists/' . urlencode($list['name']) . '">' . $list['name'] . '</a></td><td>' . $list['create_time'] . '</td><td>' . $list['email_count'] . '</td></tr>';
      }
      $html .= '</table>';
    }
  }
  catch (Sailthru_Client_Exception $e) {
    watchdog('Sailthru - Lists', 'Could not get connect to Sailthru ' . $e);
    drupal_set_message("<strong>Oh Snap!</strong> I couldn't get the lists from Sailthru");
  }
  return $html;
}

/**
 * Build out the UI for the list detail screen.
 */
function sailthru_build_list_details_ui($list_name = '') {

  $html = '';
  if ($list_name != '') {
    $sailthru = sailthru_get_client();
    try {
      $list_detail = $sailthru->apiGet('list', array('list' => $list_name));
      $html .= '<div class="st_list_details">';
      $html .= '<h2>' . $list_detail['list'] . '</h2>';
      $html .= '<div  class="st_list_public_name">Public Name:' . $list_detail['public_name'] . '</div>';
      $html .= '<div class="st_list_created">Created: ' . $list_detail['create_time'] . '</div>';
      $html .= '<div class="st_list_count">Subscribers: ' . $list_detail['count'] . '</div>';
      $html .= '<div class="st_list_type">List Type: ' . $list_detail['type'] . '</div>';
      $html .= '<div class="st_list_primary">' . $list_detail['primary'] == TRUE ? 'Primary List' : 'Secondary List' . '</div>';
      $html .= '<div class="st_list_link"><a href="https://my.sailthru.com/list?list=' . urlencode($list_detail['list']) . '" target="_blank">Manage this list in Sailthru</a></div>';
      $html .= '</div>';
    }
    catch (Sailthru_Client_Exception $e) {
      watchdog('Sailthru - List Details', 'Could not get connect to Sailthru  %exception', array(' %exception' => $e));
      drupal_set_message("<strong>Oh Snap!</strong> I couldn't get the list details from Sailthru");
    }
  }
  return $html;
}
