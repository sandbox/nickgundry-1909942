<?php

/**
 * @file
 * Main administration screen for Sailthru Integration.
 */

/**
 * Provides Settings Form.
 */
function sailthru_admin_form() {
  $form = array();
  $sailthru = sailthru_get_client(TRUE, TRUE);
  $templates = array();

  if ($sailthru) {
    $templates = $sailthru->apiGet('template', array());
    $title = t('API Configuration');
  }
  else {
    $title = t('Add API Configuration');
  }

  $form['sailthru_intro'] = array(
    '#markup' => '<div id="sailthru-intro"><div style="margin-top:20px"><p>Thanks for installing the Sailthru module, as well as the configuration options in the Sailthru admin area you can view information for each user on their profile pages.</p><p>The Horizon script will be automatically added to every page, if you already have this set in a template it can be removed and managed by this module.</p></div></div>');

  // Sailthru requires jQuery 1.5.1 or higher to run, is it installed?
  if (module_exists('jquery_update')) {
    $form['sailthru_jquery_update'] = array(
      '#markup' => 'The Sailthru module requires jQuery 1.5.1 or greater.You have jQuery Update installed, so everything should be fine.',
    );
  }
  else {

    // Allows the user to choose what version of jQuery should be used.
    $form['sailthru_jquery'] = array(
      '#type' => 'fieldset',
      '#title' => t('Update jQuery'),
      '#collapsible' => FALSE,
    );
    $form['sailthru_jquery']['intro'] = array(
      '#markup' => t("The Sailthru module requires jQuery 1.5.1 or greater. You can specify a version of jQuery you'd like to use on your site below. <p>This will load using <code>page_js_alter()</code> replacing Drupal\'s standard version. Theme developers can override this if necessary.</p>"),
    );
    $options = array(
      '' => '-- Select --',
      '1.9.0' => 'jQuery 1.9.0',
      '1.8.3' => 'jQuery 1.8.3',
      '1.8.2' => 'jQuery 1.8.2',
      '1.8.1' => 'jQuery 1.8.1',
      '1.8.0' => 'jQuery 1.8.0',
      '1.7.2' => 'jQuery 1.7.2',
      '1.7.1' => 'jQuery 1.7.1',
      '1.7.0' => 'jQuery 1.7.0',
      '1.6.4' => 'jQuery 1.6.4',
      '1.6.3' => 'jQuery 1.6.3',
      '1.6.2' => 'jQuery 1.6.2',
      '1.6.1' => 'jQuery 1.6.1',
      '1.6.0' => 'jQuery 1.6.0',
      '1.5.2' => 'jQuery 1.5.2',
    );
    $form['sailthru_jquery']['sailthru_jquery_version'] = array(
      '#type' => 'select',
      '#title' => t('Version'),
      '#options' => $options,
      '#default_value' => variable_get('sailthru_jquery_version', ''),
      '#required' => FALSE,
    );
  }

  if (isset($templates['error'])) {
    $form['sailthru_check_status'] = array(
      '#markup' => '<div class="messages error">Your API key is invalid, please check and try again.</div>',
    );
  }

  $form['sailthru'] = array(
    '#type' => 'fieldset',
    '#title' => $title,
    '#collapsible' => FALSE,
  );

  if ($sailthru) {
    $form['sailthru']['intro'] = array(
      '#markup' => '<p>Your API keys are set below.</p> ',
    );
  }
  else {
    $form['sailthru']['intro'] = array(
      '#markup' => '<p>You need your API details. <a href="https://my.sailthru.com/settings_api">Login to your Sailthru account</a> to view them.</p> ',
    );
  }

  $form['sailthru']['sailthru_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#description' => t('Enter your Sailthru API Key'),
    '#required' => TRUE,
    '#default_value' => variable_get('sailthru_api_key', ''),
  );

  $form['sailthru']['sailthru_api_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('API Secret'),
    '#description' => t('Enter your Sailthru API Secret'),
    '#required' => TRUE,
    '#default_value' => variable_get('sailthru_api_secret', ''),
  );

  $form['sailthru']['sailthru_horizon_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Horizon Domain'),
    '#description' => t('Add your Sailthru Horizon domain'),
    '#required' => TRUE,
    '#default_value' => variable_get('sailthru_horizon_domain', ''),
  );

  $form['sailthru']['sailthru_api_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Sailthru API URI'),
    '#description' => t('URI of the Sailthru API'),
    '#required' => TRUE,
    '#default_value' => variable_get('sailthru_api_uri', 'https://api.sailthru.com'),
  );

  if ($sailthru) {
    $required = TRUE;
  }
  else {
    $required = FALSE;
  }

  // Template.
  if (isset($templates['templates'])) {
    $options = array('' => '-- Select --');
    $tpl_templates = '<p style="margin-bottom:15px">The template you select is used for all Drupal system emails. In the template make sure you have a variable <code>{content}</code> as this is used to display the content and that <code>{subject}</code> is set as your subject line in your template settings at my.sailthru.com</p>';
    foreach ($templates['templates'] as $template) {
      $options[$template['name']] = $template['name'];
    }
  }
  else {
    $tpl_templates = '<p class="error">Before you can enable Sailthru, you\'ll need to select a template to use. This drop down will populate options when you add and save your API keys.</p>';
    $options = array('' => '-- Configure Your API Key First--');
  }

  $form['sailthru']['sailthru_default_template'] = array(
    '#type' => 'select',
    '#title' => t('Select the default template to use for all system emails'),
    '#options' => $options,
    '#default_value' => variable_get('sailthru_default_template', ''),
    '#suffix' => t($tpl_templates),
    '#required' => $required,
  );

  $checklist_vocab_array = array();
  if (function_exists('taxonomy_get_vocabularies')) {
    // Get the taxonomy vocabularies.
    $vocabulary = taxonomy_get_vocabularies();
    $checklist_vocab_array = array('0' => '--Select--');

    foreach ($vocabulary as $item) {
      $key = $item->vid;
      $value = $item->name;
      $checklist_vocab_array[$key] = $value;
    }

    $form['sailthru']['sailthru_taxonomy'] = array(
      '#type' => 'select',
      '#title' => t('Taxonomy for Tags'),
      '#options' => $checklist_vocab_array,
      '#default_value' => variable_get('sailthru_taxonomy', ''),
      '#required' => $required,
      '#element_validate' => array('sailthru_validate_taxonomy'),
      '#field_prefix' => '<p style="margin-bottom:10px">To provide the best recommendations for your users we require that you tag your content. Select or create a taxonomy you wish to use for our Horizon tags below. For more information check out the documentation on Horizon Tags</p>',
    );
    $form['#submit'][] = 'sailthru_admin_form_submit';
  }
  else {
    drupal_set_message(t('Enable the taxonomy module, and create a vocabulary to use for tags'), 'error');
  }
  return system_settings_form($form);
}

/**
 * Handles submission of configuration form.
 */
function sailthru_admin_form_submit() {
  variable_set('sailthru_configured', TRUE);
  // If we're all good NOW override the default mail system.
  variable_set('mail_system', array('default-system' => 'SailthruMailSystem'));
}

/**
 * Validates the taxonomy list.
 */
function sailthru_validate_taxonomy($element) {
  if (($element['#value'] == 0)) {
    form_set_error('sailthru', t('Select a taxonomy'));
  }
}
