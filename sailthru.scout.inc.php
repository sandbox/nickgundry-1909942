<?php
/**
 * @file
 * File for rendering scout. This will eventually contain more functionality.
 */

/**
 * Gets the html to render Scout on the page.
 */
function sailthru_get_scout_page() {
  $html = '<div id="sailthru-scout"><div class="loading">' . t('Loading, please wait') . '</div></div>';
  return $html;
}
