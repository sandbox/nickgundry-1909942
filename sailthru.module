<?php

/**
 * @file
 * Main module file for Sailthru Integration.
 *
 * Provides module configuration and help functionality.
 */

 /**
  * Constructor for Sailthru client.
  */
function sailthru_get_client($reveal_php_version = TRUE, $is_config = FALSE) {

  if ($is_config) {
    $st = sailthru_validate_install(TRUE);
  }
  else {
    $st = sailthru_validate_install(FALSE);
  }

  $api_key = variable_get('sailthru_api_key', '');
  $api_secret = variable_get('sailthru_api_secret', '');
  $api_uri = variable_get('sailthru_api_uri', 'https://api.sailthru.com');
  $horizon_domain = variable_get('sailthru_horizon_domain', '');

  if (empty($api_key)) {
    watchdog('sailthru', 'The Sailthru API Key has not been configured. Please visit the <a href="!url">configuration page</a>.', array('!url' => url('admin/config/system/sailthru_client')));
    drupal_set_message(t('Add your Sailthru API key'), 'error', FALSE);
    return FALSE;
  }
  if (empty($api_secret)) {
    watchdog('sailthru', 'The Sailthru API Secret has not been configured. Please visit the <a href="!url">configuration page</a>.', array('!url' => url('admin/config/system/sailthru_client')));
    drupal_set_message(t('Add your Sailthru API Secret'), 'error', FALSE);
    return FALSE;
  }

  if (empty($horizon_domain)) {
    watchdog('sailthru', 'The Horizon domain has not been configured. Please visit the <a href="!url">configuration page</a>.', array('!url' => url('admin/config/system/sailthru_client')));
    drupal_set_message(t('Please add your Sailthru Horizon domain'), 'error', FALSE);
    return FALSE;
  }

  return new Sailthru_client($api_key, $api_secret, $api_uri, $reveal_php_version);

}

/**
 * Implements hook_help().
 */
function sailthru_help($path, $arg) {
  switch ($path) {
    case "admin/help#sailthru":
      return check_markup(file_get_contents(dirname(__FILE__) . "/README.txt"));
  }
}

/**
 * Implements hook_permission().
 */
function sailthru_permission() {
  return array(
    'access sailthru configuration' => array(
      'title' => t('Access Sailthru Configuration'),
    ),
  );
}

/**
 * Checks that the API is configured and that there is a default template set.
 */
function sailthru_validate_install($installation = FALSE) {

  if (function_exists('libraries_get_path')) {
    // Check that the Sailthru library is installed.
    $library_path = libraries_get_path('sailthru');
    if (file_exists("$library_path/Sailthru_Client.php")) {

      $_sailthru_loaded = TRUE;
      require_once "$library_path/Sailthru_Client.php";

      if (file_exists("$library_path/Sailthru_Util.php")) {
        require_once "$library_path/Sailthru_Util.php";
      }
      else {
        $_sailthru_loaded = FALSE;
      }

      if (file_exists("$library_path/Sailthru_Client_Exception.php")) {
        require_once "$library_path/Sailthru_Client_Exception.php";
      }
      else {
        $_sailthru_loaded = FALSE;
      }
    }
    else {
      $_sailthru_loaded = FALSE;
    }
  }
  else {
    $_sailthru_loaded = FALSE;
  }

  if (arg(0) == 'admin' && arg(1) == 'sailthru' && arg(2) != 'download') {
    if (!$_sailthru_loaded) {
      drupal_set_message(t("Download the Sailthru client library to enable Sailthru features."));
      drupal_goto('admin/sailthru/download');
    }
  }

  $is_valid = FALSE;
  $api_key = variable_get('sailthru_api_key', '');
  $api_secret = variable_get('sailthru_api_secret', '');
  $tpl = variable_get('sailthru_default_template', '');

  if ($installation) {
    if ($api_key != '' && $api_secret != '') {
      $is_valid = TRUE;
    }
    else {
      $is_valid = FALSE;
    }
  }
  else {
    if ($api_key != '' && $api_secret != '' && $tpl != '') {
      $is_valid = TRUE;
    }
    else {
      $is_valid = FALSE;
    }
  }

  return $is_valid;
}

/**
 * Checks that the the Sailthru is able to return an API call.
 */
function sailthru_check_sailthru() {

  try {
    $sailthru = sailthru_get_client();
    if ($sailthru) {
      $data = array();
      $tpl = $sailthru->apiGet('template', $data);
      if (isset($tpl['templates'])) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    else {
      return FALSE;
    }
  }
  catch (Sailthru_Client_Exception $e) {
    watchdog('Sailthru client not configured correctly' . $e . '', WATCHDOG_WARNING);
  }
}


/**
 * Implements hook_menu().
 *
 * This hook declares the menu structure for used by the module.
 */
function sailthru_menu() {

  $items['admin/sailthru'] = array(
    'title' => 'Sailthru',
    'description' => 'Configure API keys and products',
    'page callback' => 'drupal_get_form',
    'access arguments' => array('access sailthru configuration'),
    'page arguments' => array('sailthru_admin_form'),
    'weight' => -50,
    'type' => MENU_NORMAL_ITEM,
    'file' => 'sailthru.admin.inc.php',
  );

  $items['admin/sailthru/config'] = array(
    'title' => 'Configuration',
    'description' => 'Configure API keys and products',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sailthru_admin_form'),
    'access arguments' => array('access sailthru configuration'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -50,
    'file' => 'sailthru.admin.inc.php',
  );

  $items['admin/sailthru/download'] = array(
    'title' => 'Installation',
    'description' => 'Install the necessary libraries',
    'page callback' => 'sailthru_install_download',
    'access arguments' => array('access sailthru configuration'),
    'weight' => 10,
    'type' => MENU_LOCAL_TASK,
    'file' => 'sailthru.applications.inc.php',
  );

  $items['admin/sailthru/concierge'] = array(
    'title' => 'Concierge',
    'description' => 'Configure Concierge',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sailthru_concierge_config'),
    'access arguments' => array('access sailthru configuration'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 0,
    'file' => 'sailthru.applications.inc.php',
  );

  $items['admin/sailthru/scout'] = array(
    'title' => 'Scout',
    'description' => 'Configure Scout',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sailthru_scout_config'),
    'access arguments' => array('access sailthru configuration'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
    'file' => 'sailthru.applications.inc.php',
  );

  $items['admin/sailthru/applications'] = array(
    'title' => 'Helper Blocks',
    'description' => 'Helpers such as newsletter subscription blocks',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sailthru_apps_config_form'),
    'access arguments' => array('access sailthru configuration'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
    'file' => 'sailthru.applications.inc.php',
  );

  $items['admin/sailthru/lists'] = array(
    'title' => 'Lists',
    'description' => 'View and Manage Lists',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sailthru_lists'),
    'access arguments' => array('access sailthru configuration'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'sailthru.applications.inc.php',
    'weight' => 3,
  );

  $items['admin/sailthru/lists/%view'] = array(
    'title' => 'Lists',
    'description' => 'View and Manage Lists',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sailthru_lists', '3'),
    'access arguments' => array('access sailthru configuration'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'sailthru.applications.inc.php',
    'weight' => 4,
  );

  $items['sailthru/scout'] = array(
    'title' => 'Sailthru Scout',
    'description' => 'Sailthru Scout',
    'page callback' => 'sailthru_get_scout_page',
    'access arguments' => array('access content'),
    'weight' => -50,
    'type' => MENU_NORMAL_ITEM,
    'file' => 'sailthru.applications.inc.php',
  );

  return $items;
}

/**
 * Implements hook_block_info().
 *
 * This hook declares what blocks are provided by the module.
 */
function sailthru_block_info() {

  $has_list = variable_get('user_subscribe_list');
  if ($has_list != 0 || $has_list != '') {
    $blocks['user_subscribe_block'] = array(
      // info: The name of the block.
      'info' => t('Sailthru Email Subscribe'),
      'cache' => DRUPAL_CACHE_PER_ROLE,
    );
  }

  $blocks['sailthru_recommends'] = array(
    // info: The name of the block.
    'info' => t('Sailthru Recommendations'),
    'cache' => DRUPAL_CACHE_PER_ROLE,
  );
  return $blocks;
}

/**
 * Implements hook_block_configure().
 *
 * This hook declares configuration options for blocks provided by this module.
 */
function sailthru_block_configure($delta = '') {

  $form = array();
  if ($delta == 'user_subscribe_block') {
    $form['sailthru_subscribe']['user_subscribe_name_fields'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable first & last name fields'),
      '#default_value' => variable_get('sailthru_user_subscribe_name_fields', ''),
    );
    $has_list = variable_get('sailthru_user_subscribe_list');

    if (!$has_list || $has_list == '') {

      $form['sailthru_subscribe'] = array(
        '#type' => 'fieldset',
        '#title' => t('Newsletter List'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );

      try {
        $sailthru = sailthru_get_client();
        $lists = $sailthru->apiGet('list', array());
        $options = array('');
        foreach ($lists['lists'] as $list) {
          $options[$list['name']] = $list['name'];
        }

        $form['sailthru_subscribe']['sailthru_user_subscribe_list'] = array(
          '#type' => 'select',
          '#title' => t('Add subscribers to list'),
          '#options' => $options,
          '#default_value' => variable_get('sailthru_user_subscribe_list', ''),
          '#element_validate' => array('sailthru_validate_list'),
        );
      }
      catch (Sailthru_Client_Exception $e) {
        watchdog('Sailthru - Lists', 'Could not get lists from Sailthru ' . $e . '');
        drupal_set_message(t("<strong>Oh Snap!</strong> I couldn't get the lists from Sailthru"));
      }

    }
  }

  return $form;

}

/**
 * Validate subscription list.
 */
function sailthru_validate_list($element) {
  if (($element['#value'] != '')) {
    form_set_error('sailthru_subscribe', t('Select a list that subscribers will be added to.'));
  }
}


/**
 * Loads a newer version of Jquery on the public pages.
 */
function sailthru_js_alter(&$javascript) {

  // Check to see if jquery update is installed, if so leave it alone.
  if (!module_exists('jquery_update')) {
    $version = variable_get('sailthru_jquery_version', '');
    if ($version != '') {
      if (arg(0) != 'admin') {
        $javascript['misc/jquery.js']['data'] = '//ajax.googleapis.com/ajax/libs/jquery/' . $version . '/jquery.min.js';
      }
    }
  }
}

/**
 * Loads Sailthru Horizon Javascript at the bottom of each page into the footer.
 */
function sailthru_page_alter() {

  $horizon_domain = variable_get('sailthru_horizon_domain', '');
  $concierge_enabled = variable_get('sailthru_concierge_enabled', FALSE);
  $scout_enabled = variable_get('sailthru_scout_enabled', FALSE);
  $is_admin = arg(0) == 'admin' ? TRUE : FALSE;
  $st_setup = sailthru_validate_install();

  // Check if we're ready. If so then load up Sailthru.
  if ($st_setup) {
    if ($is_admin) {
      $concierge_enabled = FALSE;
    }

    if ($is_admin) {
      drupal_add_css(drupal_get_path('module', 'sailthru_') . '/admin.css');
    }

    switch ($concierge_enabled) {

      case "1":
        $display = array();
        $paths = variable_get('sailthru_concierge_paths', '');
        $pages[] = request_path();
        $pages[] = current_path();

        // Compare the lowercase internal and lowercase path alias (if any).
        foreach ($pages as $page) {
          $display[] = drupal_match_path($page, $paths);
        }

        // Check to see if concierge is enabled on this path.
        if (in_array(TRUE, $display)) {

          // Check if the css path starts with a slash, if so add the site url.
          if (substr(variable_get('sailthru_concierge_css_path'), 0, 1) == '/') {
            $css_path = $GLOBALS['base_url'] . check_plain(variable_get('sailthru_concierge_css_path'));
          }
          else {
            $css_path = check_plain(variable_get('sailthru_concierge_css_path'));
          }

          $css_path = trim($css_path) != '' ? $css_path : 'https://ak.sail-horizon.com/horizon/recommendation.css';

          // Generate the horizon code to be inserted in the page footer.
          $horizon_code = "Sailthru.setup({
                              domain: ' " . $horizon_domain . " ', concierge: { from: '" . variable_get('sailthru_concierge_from', 'bottom') . "',
                              threshold: '" . check_plain(variable_get('sailthru_concierge_threshold', '400')) . "',
                              filter: '" . check_plain(variable_get('sailthru_concierge_tags', '')) . "',
                              delay: '" . check_plain(variable_get('sailthru_concierge_delay', '')) . "',
                              cssPath: '" . $css_path . "'
                            }
          });";
        }
        else {
          $horizon_code = "Sailthru.setup({
                domain: '" . $horizon_domain . "',
            });";
        }

        break;

      default;

      $horizon_code = "Sailthru.setup({
        domain: '" . $horizon_domain . "',
      });";

    }

    // Add the Horizon Javascript to the page.
    $horizon_code = '';
    drupal_add_js("
                    function loadHorizon() {
                          var s = document.createElement('script');
                          s.type = 'text/javascript';
                          s.async = true;
                          s.src = location.protocol + '//ak.sail-horizon.com/horizon/v1.js';
                          var x = document.getElementsByTagName('script')[0];
                          x.parentNode.insertBefore(s, x);
                      }
                      loadHorizon();
                      var oldOnLoad = window.onload;
                      window.onload = function() {
                          if (typeof oldOnLoad === 'function') {
                              oldOnLoad();
                          }
                          " . $horizon_code . "
                      };

        ",
      array(
        'type' => 'inline',
        'scope' => 'footer'));

    $render_item = variable_get('sailthru_scout_render', '');

    if ($render_item != '') {
      $render_item = 'renderItem:' . $render_item;
    }

    drupal_add_js('//ak.sail-horizon.com/scout/v1.js', array('scope' => 'footer', 'type' => 'external'));

    // Add the Scout Javascript to the page.
    drupal_add_js("
          SailthruScout.setup({
            domain: '" . check_plain(variable_get('sailthru_horizon_domain')) . "',
            numVisible: '" . check_plain(variable_get('sailthru_scout_num_visible', 10)) . "',
            includeConsumed: '" . check_plain(variable_get('sailthru_scout_include_consumed', FALSE)) . "',
            " . $render_item . "
          });",
      array(
        'type' => 'inline',
        'scope' => 'footer'));
  }
}

/**
 * Gets the tags to be used for the Sailthru Meta Tags.
 */
function sailthru_get_node_tags($nid) {
  $tags = array();
  $result = db_query("SELECT ttd.tid, ttd.vid, ttd.name, ttd.description, ttd.weight
  FROM taxonomy_term_data AS ttd
  INNER JOIN taxonomy_index ti ON ti.tid = ttd.tid
  WHERE ti.nid= :nid
  ORDER BY ttd.weight, ttd.name", array(':nid' => $nid));

  foreach ($result as $tag) {
    $tags[] = check_plain($tag->name);
  }
  return $tags;
}

/**
 * Implements template_preprocess_html().
 */
function sailthru_preprocess_html(&$vars) {

  // Get the tags for the page and add them to the meta tags.
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    $node = node_load(arg(1));

    $st_tags = array(
      '#tag' => 'meta',
      '#attributes' => array(
        'name'    => 'sailthru.tags',
        'content' => implode(sailthru_get_node_tags($node->nid), ','),
      ),
    );
    drupal_add_html_head($st_tags, 'sailthru_tags');

    $st_author = array(
      '#tag' => 'meta',
      '#attributes' => array(
        'name'    => 'sailthru.author',
        'content' => $node->name,
      ),
    );
    drupal_add_html_head($st_author, 'sailthru_author');

    $st_title = array(
      '#tag' => 'meta',
      '#attributes' => array(
        'name'    => 'sailthru.title',
        'content' => $node->title,
      ),
    );
    drupal_add_html_head($st_title, 'sailthru_title');

    $st_date = array(
      '#tag' => 'meta',
      '#attributes' => array(
        'name'    => 'sailthru.date',
        'content' => date("c", $node->created),
      ),
    );
    drupal_add_html_head($st_date, 'sailthru_date');
  }

}

/**
 * Implements hook_login().
 *
 * Captures the last login of the user and adds it to the Sailthru profile
 */
function sailthru_user_login(&$edit, $account) {

  $sailthru = sailthru_get_client();
  $id = $account->mail;
  $options = array(
    'login' => array(
      'user_agent' => $_SERVER['HTTP_USER_AGENT'],
      'key' => 'email',
      'ip' => ip_address(),
      'site' => check_plain(variable_get('site_name', '')),
    ),
    'fields' => array('keys' => 1),
  );
  try {
    if ($sailthru) {
      $st = $sailthru->saveUser($id, $options);
    }
  }
  catch (Sailthru_Client_Exception $e) {
    watchdog('Sailthru - User login Capture', 'last login for ' . $id . ' was not captured');
  }
}


/**
 * Implements hook_block_save().
 *
 * This hook declares how the configured options for a block
 * provided by this module are saved.
 */
function sailthru_block_save($delta = '', $edit = array()) {

  if ($delta == 'user_subscribe_block') {
    variable_set('sailthru_user_subscribe_list', $edit['sailthru_user_subscribe_list']);
    variable_set('sailthru_user_subscribe_name_fields', $edit['sailthru_user_subscribe_name_fields']);
  }
}

/**
 * Implements hook_block_view().
 *
 * This hook generates the contents of the blocks themselves.
 */
function sailthru_block_view($delta = '') {
  // The $delta parameter tells us which block is being requested.
  $block = NULL;

  switch ($delta) {
    case 'user_subscribe_block':
      $block['content'] = drupal_get_form('sailthru_subscribe_form');
      break;

    case 'sailthru_recommends':
      $block['title'] = "Recommended For You";
      $block['content'] = sailthru_get_scout_block();
      break;
  }
  return $block;
}

/**
 * Generates a div to render Scout.
 */
function sailthru_get_scout_block() {
  $html = '<div id="sailthru-scout"></div>';
  return $html;
}

/**
 * Creates a user subscription form.
 *
 * This form is used in a block to capture a user subscription.
 */
function sailthru_subscribe_form() {

  $form = array();
  $form['sailthru_subscribe']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#required' => TRUE,
    '#default_value' => '',
  );

  $show_name = variable_get('user_subscribe_name_fields');
  if ($show_name) {
    $form['sailthru_subscribe']['first_name'] = array(
      '#type' => 'textfield',
      '#title' => t('First Name'),
      '#required' => TRUE,
      '#default_value' => '',
    );

    $form['sailthru_subscribe']['last_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Last Name'),
      '#required' => TRUE,
      '#default_value' => '',
    );
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Subscribe'));

  return $form;

}

/**
 * Implements form_submit().
 *
 * This hook is fired when the subscribe form is submitted
 */
function sailthru_subscribe_form_submit($form, &$form_state) {

  $sailthru = sailthru_get_client();
  $vars = array();
  $email = isset($form['sailthru_subscribe']['email']['#value']) ? $form['sailthru_subscribe']['email']['#value'] : '';
  $first_name = isset($form['sailthru_subscribe']['first_name']['#value']) ? $form['sailthru_subscribe']['first_name']['#value'] : '';
  $last_name = isset($form['sailthru_subscribe']['last_name']['#value']) ? $form['sailthru_subscribe']['last_name']['#value'] : '';

  if ($first_name != ''  || $last_name != '') {
    $vars = array('first_name' => $first_name, 'last_name' => $last_name);
  }
  try {
    $lists = array(variable_get('user_subscribe_list') => 1);
    $st = $sailthru->setEmail($email, $vars, $lists);
    drupal_set_message(t('Thanks for subscribing'));
  }
  catch (Sailthru_Client_Exception $e) {
    watchdog('Sailthru - Subscriber', 'failed to capture subscriber ' . $email . '');
  }
}

/**
 * Gets the available Sailthru templates from the Sailthru account.
 */
function sailthru_get_templates($sailthru) {
  $options = array();
  if ($sailthru) {
    $templates = $sailthru->apiGet('template', array());
    $options = array('' => '-- select--');
    foreach ($templates['templates'] as $template) {
      $options[$template['name']] = $template['name'];
    }
  }
  return $options;
}

/**
 * Mail test.
 */
function sailthru_mail($key, &$message, $params) {
  $language = $message['language'];

  switch ($key) {
    case 'test':
      $message['subject'] = t('Drupal test email', array(), $language->language);
      $message['body'] = t('If you receive this message it means your site is capable of sending email.', array(), $language->language);
      break;
  }
}

/**
 * Adds user profile information from Sailthru to the user profile in Drupal.
 */
function sailthru_user_view($account, $view_mode, $langcode) {

  try {
    $sailthru = sailthru_get_client();
    $fields = array(
      'activity' => 1,
      'keys' => 1,
      'vars' => 1,
      'lists' => 1,
      'engagement' => 1,
      'optout' => 1,
      'smart_lists' => 1);
    $data = array('id' => $account->mail, 'key' => 'email', 'fields' => $fields);
    $user = $sailthru->apiGet('user', $data);

    $account->content['sailthru']['user'] = array(
      '#type' => 'user_profile_item',
      '#title' => t('<div id="sailthru-profile-header"><h3>Sailthru User Profile</h3></div>'),
      '#markup' => sailthru_user_profile_ui($user),
      '#attributes' => array('class' => 'sailthru-profile'),
    );
  }
  catch (Sailthru_Client_Exception $e) {
    watchdog('Sailthru - Subscriber', 'could not get user profile data from Sailthru ' . $e . '');
  }

}

/**
 * Builds the Sailthru profile HTML used in the Drupal profile screen.
 */
function sailthru_user_profile_ui($user) {

  if (!isset($user['error'])) {
    $html = '<div class="sailthru-user-profile">';
    $html .= '<div class="sailthru-user-email"><strong>Email: </strong>' . check_plain(strtolower($user['keys']['email'])) . '</div>';
    $html .= '<div class="sailthru-user-engagement"><strong>Engagement: </strong>' . ucwords(check_plain($user['engagement'])) . '</div>';
    $html .= '<div class="sailthru-user-optout"><strong>Optout Email: </strong>' . ucwords(check_plain($user['optout_email'])) . '</div>';

    if (count($user['smart_lists']) > 0) {
      $html .= '<div class="sailthru-lists"><h4>Smart Lists</h4><table><tr><th>List Name</th><th>Date</th></tr>';
      foreach ($user['lists'] as $list => $date) {
        $html .= '<tr><td>' . check_plain($list) . '</td><td>' . check_plain($date) . '</td></tr>';
      }
      $html .= '</table></div>';
    }
    else {
      $html .= "<h4>Smart Lists</h4><p>The user is not subscribed to any smart lists</p>";
    }

    // Lists.
    if (count($user['lists']) > 0) {
      $html .= '<div class="sailthru-lists"><h4>Lists</h4><table><tr><th>List Name</th><th>Date</th></tr>';
      foreach ($user['lists'] as $list => $date) {
        $html .= '<tr><td>' . check_plain($list) . '</td><td>' . check_plain($date) . '</td></tr>';
      }
      $html .= '</table></div>';
    }
    else {
      $html .= "<h4>Lists</h4><p>The user is not subscribed to any lists</p>";
    }

    if (count($user['vars']) > 0) {
      $html .= '<div class="sailthru-vars"><h4>Vars</h4>';

      if (count($user['vars']) > 0) {
          $html .= '<table>';
          $html .= '<tr><th>Var</th><th>Value</th></tr>';
          foreach ($user['vars'] as $var => $value) {
            $html .= '<tr class="sailthru-user-var"><td><strong>' . check_plain($var) . '</strong></td><td> ' . check_plain($value) . '</td></tr>';
          }
      }

      $html .= '</table></div>';
    }

    $html .= '</div>';
  }
  else {
    $html = '<div class="sailthru-user-profile"><p>' . t('We cannot find a record of this user in your Sailthru account.') . '</p></div>';
  }
  return $html;
}