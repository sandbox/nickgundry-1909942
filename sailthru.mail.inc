<?php
/**
 * @file
 * Implements Drupal MailSystemInterface
 */

class SailthruMailSystem implements MailSystemInterface {

  /**
   * Formats the message.
   */
  public function format(array $message) {
    // Join message array.
    $message['body'] = implode("\n\n", $message['body']);
    return $message;
  }

  /**
   * Overides drupal's mail.
   */
  public function mail(array $message) {

    $template = variable_get('sailthru_default_template');
    $sailthru = sailthru_get_client();
    $message['params']['subject'] = $message['subject'];
    if (isset($message['body'])) {
      $message['params']['content'] = $message['body'];
    }
    $message['params']['content'] = check_markup($message['params']['content']);
    try {
      $st = $sailthru->send($template, $message['to'], $vars = $message['params'], $options = $message['headers']);
      $message['result'] = $st;
      return $message;
    }
    catch (Sailthru_Client_Exception $e) {
      watchdog('Sailthru', 'Mail delivery failed with message ' . $e);
      drupal_set_message(t('Email could not be sent'));
    }
  }

}
