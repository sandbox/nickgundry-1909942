Sailthru Configuration
===============================================================================

Configure a template that Drupal will use in https://my.sailthru.com/templates
The template HTML needs to have a variable {content}. This will be replaced with
content used in Drupal system emails.

In the template 'Basics' tab enter {subject} as the subject line

Drupal Configuration
===============================================================================

Installation:

Download the Sailthru PHP5 Client Library from
https://github.com/sailthru/sailthru-php5-client/archive/master.zip

1. Extract the Sailthru Client Library and move the sailthru
   folder to /sites/all/libraries/
   create the libraries folder if it does not exist. The path to the
   the client file should be /sites/all/modules/sailthru/Sailthru_Client.php
2. Download and install the Drupal module in sites/all/modules
3. Enable the Sailthru Module at Administer-> Modules
4. Once installed, enter your API Key and Secret at /admin/sailthru
5. Select the template to use in system emails. This will need to match the one
   created in the Sailthru Configuration steps above.

What this module does:

* Automatically adds Horizon Script to all pages
* Automatically tags all pages
* All Emails being sent by Drupal are pumped through Sailthru
* User Logins automatically tracked
* Create a "Sailthru Scout" page at /sailthru/scout. This could be aliased
* Creates a recommended block
* Adds a block for email subscriptions and a list subscribers should be added to
* Adds Configuration screens for Concierge and Scout.

IF THERE ARE ANY QUESTIONS PLEASE EMAIL nick@sailthru.com
